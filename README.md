# IOTA - A State-of-the-art Distributed Ledger Technology (DLT)
## Lab Experiment

**Developed and conducted by Andreas Markwart**

**Module: Communication System Security**

**Master programme: Information Engineering and Computer Sciences, Summer semester 2019**

**Rhine-Waal University of Applied Sciences**

**Lecturer: Prof. Dr.-Ing. Ulrich Greveler**


## Introduction
The lab experiment utilizes the tangle as an immutable ledger. As such, data can be 
saved in the tangle as a message fragment within a transaction. In this way, the 
tangle acts as a trusted, distributed database. One application for such a system 
is proof of existence or more general trusted timestamping. Proof of existence asserts, 
that a file existed at a particular time in a specific status. To accomplish that goal, 
the hash value of the file instead of the file itself is commonly timestamped, as saving
big files on a trusted database would require too much unnecessary resources.